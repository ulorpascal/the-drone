require('dotenv').config();
const { Op } = require('sequelize');
const {
  success, CREATED, OK, BAD_REQUEST, CONFLICT, SERVER_ERROR, NOT_FOUND, customError,
} = require('request-response-handler');
const models = require('../database/models');
const Logger = require('../config/winston.config');
const paginate = require('../helpers/paginate');

/**
 * Handles drones
 *
 * @class DroneController
 */
class MedicationController {
  /**
   * Create Drone
   *
   * @static
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @return {*}
   * @memberof MedicationController
   */
  static async CreateMedication(req, res, next) {
    try {
        // TODO get user ID and payload
        const __uuid = req.user;
      const { name, weight, code } = req.payload;
      let image = req.payload.image;
      if(req.file) {
        image = req.file.path
      }
      // TODO create drone
      const medication = await models.Medication.create({ name, weight, code, image, userId: __uuid });
      
      return success(res, CREATED, 'Medication Created Successfully', medication);
    } catch (error) {
      return next(
        customError({
          status: SERVER_ERROR,
          message: `Try again something went wrong ${error}`,
        }),
      );
    }
  }


  /**
   * Get Medication
   *
   * @static
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @return {*}
   * @memberof DroneController
   */
   static async GetMedication(req, res, next) {
    try {
        // TODO get user ID and payload
        const __uuid = req.user;
      // TODO get drone
      const page = req.payload.page;
      const limit = process.env.PAGE_LIMIT || 10;
      const offset = (page - 1) * 10;
      const modelConfig = {
        raw: true,
        limit,
        offset,
        order: [['updatedAt', 'DESC']],
        where: {
          userId: __uuid
        }
      }
      if(req.payload.name){
        modelConfig.where.name = req.payload.name;
      }
      if(req.payload.code){
        modelConfig.where.serialNumber = req.payload.serialNumber;
      }
      if(req.payload.weight){
        modelConfig.where.weight = req.payload.weight;
      }
      if(req.payload.batteryCapacity){
        modelConfig.where.batteryCapacity = req.payload.batteryCapacity;
      }
      const medication = await models.Medication.findAll(modelConfig);
      const response = paginate(medication, 'Medications', medication.length, limit, page );
      
      return success(res, OK, 'Medication Retrieved Successfully', response);
    } catch (error) {
      return next(
        customError({
          status: SERVER_ERROR,
          message: `Try again something went wrong ${error}`,
        }),
      );
    }
  }

  /**
   * Get GetMedById
   *
   * @static
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @return {*}
   * @memberof DroneController
   */
   static async GetMedById(req, res, next) {
    try {
        // TODO get user ID and payload
        const __uuid = req.user;
      // TODO get drone
      if(!req.payload.medId) {
        return next(
          customError({
            status: NOT_FOUND,
            message: 'Medication does not exist',
          }),
        );
      }
      const modelConfig = {
        where: {
          id: req.payload.medId
        }
      }
      const medication = await models.Medication.findOne(modelConfig);
      return success(res, OK, 'Medication Retrieved Successfully', medication);
    } catch (error) {
      return next(
        customError({
          status: SERVER_ERROR,
          message: `Try again something went wrong ${error}`,
        }),
      );
    }
  }
}

module.exports = MedicationController;
