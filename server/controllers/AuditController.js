require('dotenv').config();
const { Op } = require('sequelize');
const {
  success, CREATED, OK, BAD_REQUEST, CONFLICT, SERVER_ERROR, NOT_FOUND, customError,
} = require('request-response-handler');
const models = require('../database/models');
const Logger = require('../config/winston.config');
const paginate = require('../helpers/paginate');

/**
 * Handles drones
 *
 * @class DroneController
 */
class AuditController {
  /**
   * Get Audit
   *
   * @static
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @return {*}
   * @memberof DroneController
   */
   static async GetAudits(req, res, next) {
    try {
        // TODO get user ID and payload
        const __uuid = req.user;
      // TODO get drone
      const page = req.payload.page;
      const limit = process.env.PAGE_LIMIT || 10;
      const offset = (page - 1) * 10;
      const modelConfig = {
        raw: true,
        limit,
        offset,
        order: [['updatedAt', 'DESC']],
        where: {}
      }
      if(req.payload.droneId){
        modelConfig.where.droneId = req.payload.droneId;
      }
      if(req.payload.state){
        modelConfig.where.auditId = req.payload.auditId;
      }
      if(req.payload.level){
        modelConfig.where.level = req.payload.level;
      }
      const audits = await models.Audit.findAll(modelConfig);
      const response = paginate(audits, 'Audits', audits.length, limit, page );
      
      return success(res, OK, 'Audits Retrieved Successfully', response);
    } catch (error) {
      return next(
        customError({
          status: SERVER_ERROR,
          message: `Try again something went wrong ${error}`,
        }),
      );
    }
  }

  /**
   * Get GetAuditById
   *
   * @static
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @return {*}
   * @memberof DroneController
   */
   static async GetAuditById(req, res, next) {
    try {
        // TODO get user ID and payload
        const __uuid = req.user;
      // TODO get drone
      if(!req.payload.auditId) {
        return next(
          customError({
            status: NOT_FOUND,
            message: 'Drone does not exist',
          }),
        );
      }
      const modelConfig = {
        where: {
          id: req.payload.auditId
        }
      }
      const audits = await models.Audit.findOne(modelConfig);
      return success(res, OK, 'Audit Retrieved Successfully', audits);
    } catch (error) {
      return next(
        customError({
          status: SERVER_ERROR,
          message: `Try again something went wrong ${error}`,
        }),
      );
    }
  }
}

module.exports = AuditController;
