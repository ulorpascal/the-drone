require('dotenv').config();
const { Op } = require('sequelize');
const {
  success, CREATED, OK, BAD_REQUEST, CONFLICT, SERVER_ERROR, customError,
} = require('request-response-handler');
const models = require('../database/models');
const Logger = require('../config/winston.config');
const { createToken } = require('../helpers/token');

/**
 * Handles users
 *
 * @class UserController
 */
class UserController {
  /**
   * Create User
   *
   * @static
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @return {*}
   * @memberof UserController
   */
  static async UserSignup(req, res, next) {
    try {
      const { username, email, password } = req.payload;
      // TODO create user
      const [user, created] = await models.User.findOrCreate({
        where: {
          [Op.or]: [{ username }, { email }],
        },
        defaults: { username, email, password },
      });
      if (created) {
        user.token = createToken({ __uuid: user.id, username });
        return success(res, CREATED, 'User Signup Successful', { id: user.id, username, email, token: user.token });
      }
      return next(customError({
        status: CONFLICT,
        message: 'User with the email or username already exists',
      }));
    } catch (error) {
      return next(
        customError({
          status: SERVER_ERROR,
          message: `Try again something went wrong ${error}`,
        }),
      );
    }
  }

  /**
   * Login User
   *
   * @static
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @return {*}
   * @memberof UserController
   */
  static async UserLogin(req, res, next) {
    try {
      const { email, password } = req.payload;
      // TODO get user
      const user = await models.User.findOne({
        where: { email },
      });
      if (user) {
        const checkPassword = await user.validatePassword(password);
        if (checkPassword) {
          user.token = createToken({ __uuid: user.id, username: user.username });
          return success(res, OK, 'User Login Successful', {
            id: user.id, username: user.username, email, token: user.token,
          });
        }
        return next(customError({
          status: BAD_REQUEST,
          message: 'Invalid email or password',
        }));
      }
      return next(customError({
        status: BAD_REQUEST,
        message: 'Invalid email or password',
      }));
    } catch (error) {
      return next(
        customError({
          status: SERVER_ERROR,
          message: `Try again something went wrong ${error}`,
        }),
      );
    }
  }

  /**
   * Get User
   *
   * @static
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @return {*}
   * @memberof UserController
   */
  static async GetUser(req, res, next) {
    try {
      // TODO get user
      const user = await models.User.findOne({
        where: { id: req.user },
      });

      if (user) {
        return success(res, OK, 'User Retrieved Successful', {
          id: user.id, username: user.username, email: user.email,
        });
      }
      return next(customError({
        status: BAD_REQUEST,
        message: 'Invalid email or password',
      }));
    } catch (error) {
      return next(
        customError({
          status: SERVER_ERROR,
          message: `Try again something went wrong ${error}`,
        }),
      );
    }
  }
}

module.exports = UserController;
