require('dotenv').config();
const { Op } = require('sequelize');
const {
  success, CREATED, OK, BAD_REQUEST, CONFLICT, SERVER_ERROR, NOT_FOUND, customError,
} = require('request-response-handler');
const models = require('../database/models');
const Logger = require('../config/winston.config');
const paginate = require('../helpers/paginate');

/**
 * Handles drones
 *
 * @class DroneController
 */
class DroneController {
  /**
   * Create Drone
   *
   * @static
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @return {*}
   * @memberof DroneController
   */
  static async CreateDrone(req, res, next) {
    try {
        // TODO get user ID and payload
        const __uuid = req.user;
      const { model, weightLimit, batteryCapacity, state } = req.payload;
      // TODO create drone
      const drone = await models.Drones.create({ model, weightLimit, batteryCapacity, state, userId: __uuid });
      
      return success(res, CREATED, 'Drone Created Successfully', drone);
    } catch (error) {
      return next(
        customError({
          status: SERVER_ERROR,
          message: `Try again something went wrong ${error}`,
        }),
      );
    }
  }


  /**
   * Get Drone
   *
   * @static
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @return {*}
   * @memberof DroneController
   */
   static async GetDrone(req, res, next) {
    try {
        // TODO get user ID and payload
        const __uuid = req.user;
      // TODO get drone
      const page = req.payload.page;
      const limit = process.env.PAGE_LIMIT || 10;
      const offset = (page - 1) * 10;
      const modelConfig = {
        raw: true,
        limit,
        offset,
        order: [['updatedAt', 'DESC']],
        where: {
          userId: __uuid
        }
      }
      if(req.payload.model){
        modelConfig.where.model = req.payload.model;
      }
      if(req.payload.serialNumber){
        modelConfig.where.serialNumber = req.payload.serialNumber;
      }
      if(req.payload.state){
        modelConfig.where.state = req.payload.state;
      }
      if(req.payload.weightLimit){
        modelConfig.where.weightLimit = req.payload.weightLimit;
      }
      if(req.payload.batteryCapacity){
        modelConfig.where.batteryCapacity = req.payload.batteryCapacity;
      }
      const drone = await models.Drones.findAll(modelConfig);
      const response = paginate(drone, 'Drones', drone.length, limit, page );
      
      return success(res, OK, 'Drone Retrieved Successfully', response);
    } catch (error) {
      return next(
        customError({
          status: SERVER_ERROR,
          message: `Try again something went wrong ${error}`,
        }),
      );
    }
  }

  /**
   * Get GetDroneById
   *
   * @static
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @return {*}
   * @memberof DroneController
   */
   static async GetDroneById(req, res, next) {
    try {
        // TODO get user ID and payload
        const __uuid = req.user;
      // TODO get drone
      if(!req.payload.droneId) {
        return next(
          customError({
            status: NOT_FOUND,
            message: 'Drone does not exist',
          }),
        );
      }
      const modelConfig = {
        where: {
          id: req.payload.droneId
        }
      }
      const drone = await models.Drones.findOne(modelConfig);
      return success(res, OK, 'Drone Retrieved Successfully', drone);
    } catch (error) {
      return next(
        customError({
          status: SERVER_ERROR,
          message: `Try again something went wrong ${error}`,
        }),
      );
    }
  }


  /**
   * Load Drone
   *
   * @static
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @return {*}
   * @memberof DroneController
   */
   static async LoadDrone(req, res, next) {
    try {
        // TODO get user ID and payload
        const __uuid = req.user;
      const { droneSerialNumber, medCode } = req.payload;

      // TODO get drone and med ids
      const droneModelConfig = {
        where: {
          serialNumber: droneSerialNumber
        }
      }

      const medModelConfig = {
        where: {
          code: medCode
        }
      }
      const getDrone = await models.Drones.findOne(droneModelConfig);

      const getMed = await models.Medication.findOne(medModelConfig);

      // TODO check that drone has not reached loadCapacity
      const currentLoad = getDrone.loadCapacity + getMed.weight;
      if(currentLoad > getDrone.weightLimit) {
        return next(customError({
          status: BAD_REQUEST,
          message: 'Drone has exceeded weight limit',
        }));
      }

      // TODO dont load if batteryCapacity is below 25%
      if(getDrone.batteryCapacity < 25) {
        return next(customError({
          status: BAD_REQUEST,
          message: 'Drone battery is below 25% and cant be used',
        }));
      }
      let state = 'IDLE';
      if((getDrone.batteryCapacity > 25) && (currentLoad < getDrone.weightLimit)) {
        state = 'LOADING'
      }
      if((getDrone.batteryCapacity > 25) && (currentLoad === getDrone.weightLimit)) {
        state = 'LOADED'
      }

      // TODO create dispatch
      const dispatchData = { 
        userId: __uuid, droneId: getDrone.id, medId: getMed.id }
      const dispatch = await models.Dispatch.create(dispatchData);

      // TODO update loadCapacity and batteryCapacity of drone
      getDrone.loadCapacity += getMed.weight;
      // TODO remove 10% battery capacity for each load
      getDrone.batteryCapacity -= 10;
      getDrone.state = state;
      getDrone.save();
      
      return success(res, CREATED, 'Drone Loaded Successfully', dispatch);
    } catch (error) {
      return next(
        customError({
          status: SERVER_ERROR,
          message: `Try again something went wrong ${error}`,
        }),
      );
    }
  }
}

module.exports = DroneController;
