const { Router } = require('express');
const Authentication = require('../middleware/Authentication');
const Validations = require('../middleware/Validations');
const DroneController = require('../controllers/DroneController');

const router = Router();

router
  .route('/')
  .post(Authentication.AuthenticateUser, Validations.validateDrone, DroneController.CreateDrone)
  .get(Authentication.AuthenticateUser, Validations.GetDrone, DroneController.GetDrone);

router
  .route('/:droneId')
  .get(Authentication.AuthenticateUser, Validations.GetDrone, DroneController.GetDroneById);

router
  .route('/load')
  .post(Authentication.AuthenticateUser, Validations.LoadDrone, DroneController.LoadDrone);

module.exports = router;
