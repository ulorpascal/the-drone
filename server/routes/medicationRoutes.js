const { Router } = require('express');
const Authentication = require('../middleware/Authentication');
const Validations = require('../middleware/Validations');
const MedicationController = require('../controllers/MedicationController');
const upload = require('../config/cloudinary');

const router = Router();

router
  .route('/')
  .post(Authentication.AuthenticateUser, upload.single('image'), Validations.validateMedication, MedicationController.CreateMedication)
  .get(Authentication.AuthenticateUser, Validations.GetMedication, MedicationController.GetMedication);


router
  .route('/:medId')
  .get(Authentication.AuthenticateUser, Validations.GetMedication, MedicationController.GetMedById);

module.exports = router;
