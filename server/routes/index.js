const { Router } = require('express');
const userRoutes = require('./userRoutes');
const droneRoutes = require('./droneRoutes');
const medicationRoutes = require('./medicationRoutes');
const auditRoutes = require('./auditRoutes');

const router = Router();

router.use('/user', userRoutes);

router.use('/drone', droneRoutes);

router.use('/medication', medicationRoutes);

router.use('/audit', auditRoutes);

module.exports = router;
