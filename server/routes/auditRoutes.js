const { Router } = require('express');
const Authentication = require('../middleware/Authentication');
const Validations = require('../middleware/Validations');
const AuditController = require('../controllers/AuditController');

const router = Router();

router
  .route('/')
  .get(Authentication.AuthenticateUser, Validations.GetAudit, AuditController.GetAudits);

router
  .route('/:auditId')
  .get(Authentication.AuthenticateUser, Validations.GetAudit, AuditController.GetAuditById);


module.exports = router;
