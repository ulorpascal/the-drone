const server = require('../../api/server');
const request = require('supertest')(server);
const db = require('../../database/models');
const mockUsers = require('../mock/user.mock');

const baseUrl = '/api/v1';

const userToken = { token: null }; 

describe('api/v1/user/* endpoints', () => {
    describe('Signup [POST] /api/v1/user/signup', () => {
      it('should signup user and return 201 Created', async () => {
        const res = await request
          .post(`${baseUrl}/user/signup`)
          .set('Content-Type', 'application/json')
          .send(mockUsers.validInput1);
        expect(res.status).toBe(201);
        expect(res.statusCode).toBe(201);
        expect(res.body.success).toEqual(true);
        expect(res.body.body.token);
        userToken.token = res.body.body.token;
      });
  
      it('should throw an error if any field is empty', async () => {
        const res = await request
          .post(`${baseUrl}/user/signup`)
          .set('Content-Type', 'application/json')
          .send(mockUsers.emptyData);
        expect(res.statusCode).toBe(400);
        expect(res.body.success).toEqual(false);
        expect(res.body.errors.message).toEqual('username is not allowed to be empty');
      });
  
      it('should throw an error if the email has already been used by another user', async () => {
        await request
          .post(`${baseUrl}/user/signup`)
          .set('Content-Type', 'application/json')
          .send(mockUsers.validInput1);
  
        const res = await request
          .post(`${baseUrl}/user/signup`)
          .set('Content-Type', 'application/json')
          .send(mockUsers.existingEmail);
        expect(res.body.success).toEqual(false);
        expect(res.body.errors.status).toBe(409);
        expect(res.body.errors.message).toEqual(
          'User with the email or username already exists'
        );
      });
  
      it('should throw an error if any field is invalid', async () => {
        const res = await request
          .post(`${baseUrl}/user/signup`)
          .set('Content-Type', 'application/json')
          .send(mockUsers.improperData);
        expect(res.statusCode).toBe(400);
        expect(res.body.errors.message).toEqual('email must be a valid email');
      });
    });
  
    describe('LOGIN [POST] /api/v1/user', () => {
      it('should return 200 OK', async () => {
        await request
          .post(`${baseUrl}/user/signup`)
          .set('Content-Type', 'application/json')
          .send(mockUsers.validInput1);
  
        const res = await request
          .post(`${baseUrl}/user/login`)
          .set('Content-Type', 'application/json')
          .send(mockUsers.userOneLogin);
        expect(res.statusCode).toBe(200);
        expect(res.body.success).toEqual(true);
        expect(res.body.body.token);
        userToken.token = res.body.body.token;
      });
  
      it('should throw an error when email field is empty', async () => {
        const res = await request
          .post(`${baseUrl}/user/login`)
          .set('Content-Type', 'application/json')
          .send(mockUsers.noEmail);
        expect(res.statusCode).toBe(400);
        expect(res.body.errors.message).toEqual('email is not allowed to be empty');
      });
  
      it('should throw an error when password is empty', async () => {
        const res = await request
          .post(`${baseUrl}/user/login`)
          .set('Content-Type', 'application/json')
          .send(mockUsers.noPassword);
        expect(res.statusCode).toBe(400);
        expect(res.body.errors.message).toEqual('password is not allowed to be empty');
      });
  
      it('should not let unregistered users login', async () => {
        const res = await request
          .post(`${baseUrl}/user/login`)
          .set('Content-Type', 'application/json')
          .send({ ...mockUsers.unregisteredEmail, ...mockUsers.newPassword });
        expect(res.statusCode).toBe(400);
        expect(res.body.errors.message).toEqual('Invalid email or password');
      });
    });

    describe('[GET] user details for logged in users', ()=>{
      it('should return 200 OK', async () => {
        const res = await request
          .get(`${baseUrl}/user`)
          .set('Content-Type', 'application/json')
          .set('token', userToken.token);
        expect(res.statusCode).toBe(200);
        expect(res.body.success).toEqual(true);
      });
    })
  });

  module.exports = userToken;