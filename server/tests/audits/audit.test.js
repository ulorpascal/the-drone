const server = require('../../api/server');
const request = require('supertest')(server);
const db = require('../../database/models');
const mockDrones = require('../mock/drone.mock');
const mockUsers = require('../mock/user.mock');
const mockMeds = require('../mock/medication.mock');
const auditLog = require('../../helpers/auditLog')

const baseUrl = '/api/v1';

const droneUser = { token: null };
const newDrone = { id: null, serialNumber: null, batteryCapacity: null }


describe('Log Audit', () => {
    describe('[POST] /api/v1/drone', () => {
      it('system should create audit logs for logged in drones', async () => {
        const res = await request
          .post(`${baseUrl}/user/signup`)
          .set('Content-Type', 'application/json')
          .send(mockUsers.validInput4);
        expect(res.statusCode).toBe(201);
        expect(res.body.success).toEqual(true);
        expect(res.body.body.token);
        droneUser.token = res.body.body.token;

        const drone = await request
          .post(`${baseUrl}/drone`)
          .set('Content-Type', 'application/json')
          .send(mockDrones.validInput3)
          .set('token', droneUser.token);
          newDrone.id = drone.body.body.id;
          newDrone.serialNumber = drone.body.body.serialNumber;
          newDrone.batteryCapacity = drone.body.body.batteryCapacity
          const log = await auditLog([{ droneId: newDrone.id, status: 'ACTIVE', level: newDrone.batteryCapacity }])
        expect(log).toEqual(
          expect.arrayContaining([
          expect.objectContaining({  
            droneId: newDrone.id,
            status: 'ACTIVE',
            level: newDrone.batteryCapacity           // 4
        })]
        ));
      });
    });


    describe('[GET] Audit for logged in users', ()=>{
      it('should return 200 OK', async () => {
        const res = await request
          .get(`${baseUrl}/audit`)
          .set('Content-Type', 'application/json')
          .set('token', droneUser.token);
        expect(res.statusCode).toBe(200);
        expect(res.body.success).toEqual(true);
      });
    })
  });