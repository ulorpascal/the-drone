const server = require('../../api/server');
const request = require('supertest')(server);
const db = require('../../database/models');
const mockDrones = require('../mock/drone.mock');
const mockUsers = require('../mock/user.mock');
const mockMeds = require('../mock/medication.mock');

const baseUrl = '/api/v1';

const droneUser = { token: null };
const newDrone = { id: null, serialNumber: null}
const newMed = { id: null, code: null };


describe('api/v1/drone/* endpoints', () => {
    describe('[POST] /api/v1/drone', () => {
      it('should create drone for logged in user and return 201 Created', async () => {
        const res = await request
          .post(`${baseUrl}/user/signup`)
          .set('Content-Type', 'application/json')
          .send(mockUsers.validInput2);
        expect(res.statusCode).toBe(201);
        expect(res.body.success).toEqual(true);
        expect(res.body.body.token);
        droneUser.token = res.body.body.token;

        const drone = await request
          .post(`${baseUrl}/drone`)
          .set('Content-Type', 'application/json')
          .send(mockDrones.validInput1)
          .set('token', droneUser.token);
          newDrone.id = drone.body.body.id;
          newDrone.serialNumber = drone.body.body.serialNumber;
        expect(drone.status).toBe(201);
        expect(drone.statusCode).toBe(201);
        expect(drone.body.success).toEqual(true);
        expect(drone.body.message).toEqual('Drone Created Successfully')
      });
  
      it('should throw an error if any field is empty', async () => {
        const res = await request
          .post(`${baseUrl}/drone`)
          .set('Content-Type', 'application/json')
          .send(mockDrones.emptyData)
          .set('token', droneUser.token);
        expect(res.statusCode).toBe(400);
        expect(res.body.success).toEqual(false);
        expect(res.body.errors.message).toEqual('weightLimit is required');
      });
    });


    describe('[GET] Drones for logged in users', ()=>{
      it('should return 200 OK', async () => {
        const res = await request
          .get(`${baseUrl}/drone`)
          .set('Content-Type', 'application/json')
          .set('token', droneUser.token);
        expect(res.statusCode).toBe(200);
        expect(res.body.success).toEqual(true);
      });

      it('should get drone by id and return 200 OK', async () => {
        const res = await request
          .get(`${baseUrl}/drone/${newDrone.id}`)
          .set('Content-Type', 'application/json')
          .set('token', droneUser.token);
        expect(res.statusCode).toBe(200);
        expect(res.body.success).toEqual(true);
      });
    })

    describe('[POST] Load Drones for logged in users', ()=>{
      it('should return 200 OK', async () => {

        const med = await request
          .post(`${baseUrl}/medication`)
          .set('Content-Type', 'application/json')
          .send(mockMeds.validInput2)
          .set('token', droneUser.token);
          newMed.id = med.body.body.id;
          newMed.code = med.body.body.code;
        const load = { droneSerialNumber: newDrone.serialNumber, medCode: newMed.code };
        const res = await request
          .post(`${baseUrl}/drone/load`)
          .set('Content-Type', 'application/json')
          .send(load)
          .set('token', droneUser.token);
        expect(res.statusCode).toBe(201);
        expect(res.body.success).toEqual(true);
      });
    })
  });