module.exports = {
    validInput1: {
    username: 'hulk',
      email: 'banner@yahoo.com',
      password: 'bruce banner'
    },
    validInput2: {
        username: 'mike',
      email: 'mk@yahoo.com',
      password: 'ulor mike'
    },
    validInput3: {
        username: 'frodo',
      email: 'frodo@yahoo.com',
      password: 'shire prince'
    },
    validInput4: {
      username: 'Sam',
    email: 'sam@yahoo.com',
    password: 'shire prince'
  },
    existingUsername: {
        username: 'hulk',
      email: 'dave@yahoo.com',
      password: 'dave 1234'
    },
  
    existingEmail: {
        username: 'Wayne',
      email: 'banner@yahoo.com',
      password: 'theFlash'
    },
    incompleteData: {
      email: 'annie@yahoo.com'
    },
    emptyData: {
        username: '',
      email: '',
      password: ''
    },
    improperData: {
        username: 'Wayne',
      email: 'banner',
      password: 'bruce'
    },
    unregisteredEmail: {
      email: 'notreg@getMaxListeners.com'
    },
    registeredEmail: {
      email: 'banner@yahoo.com'
    },
    newPassword: {
      password: 'abcdefghij'
    },
    userOneLogin: { email: 'banner@yahoo.com', password: 'bruce banner' },
    userTwoLogin: { email: 'mk@yahoo.com', password: 'ulor mike' },
    emptyLoginData: { email: '', password: '' },
    noEmail: { email: '', password: 'bruce banner' },
    noPassword: { email: 'mk@yahoo.com', password: '' },
    invalidEmail: { email: 'wrongEmail', password: 'bruce banner' },
    invalidPassword: { email: 'Bruce Banner', password: 'wrongPassword' },
    invalidEmailPassword: { email: 'wrongEmail', password: 'wrongPassword' }
  };