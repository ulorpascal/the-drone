module.exports = {
    validInput1: {
        model: "cruiserweight",
        weightLimit: "300",
        batteryCapacity: "40",
        state: "idle"
    },
    validInput2: {
        model: "Middleweight",
        weightLimit: "300",
        batteryCapacity: "40",
        state: "idle"
    },
    validInput3: {
        weightLimit: "400",
        batteryCapacity: "100"
    },
    existingUsername: {
        username: 'hulk',
      email: 'dave@yahoo.com',
      password: 'dave 1234'
    },
  
    existingEmail: {
        username: 'Wayne',
      email: 'banner@yahoo.com',
      password: 'theFlash'
    },
    incompleteData: {
      email: 'annie@yahoo.com'
    },
    emptyData: {
        username: '',
      email: '',
      password: ''
    },
    improperData: {
        username: 'Wayne',
      email: 'banner',
      password: 'bruce'
    },
    unregisteredEmail: {
      email: 'notreg@getMaxListeners.com'
    },
    registeredEmail: {
      email: 'banner@yahoo.com'
    }
  };