module.exports = {
    validInput1: {
        name: "covid_19",
        weight: "300",
        code: "COVID",
        image: "virus.png"
    },
    validInput2: {
      name: "ebola_18",
      weight: "300",
      code: "EBOLA",
      image: "ebola.jpeg"
    },
    validInput3: {
        weight: "300",
        batteryCapacity: "40"
    },
    emptyData: {
        username: '',
      email: '',
      password: ''
    },
    improperData: {
        name: "covid*",
        weight: "300",
        code: "mal",
        image: "pix"
    },
    unregisteredEmail: {
      email: 'notreg@getMaxListeners.com'
    },
    registeredEmail: {
      email: 'banner@yahoo.com'
    }
  };