const server = require('../../api/server');
const request = require('supertest')(server);
const db = require('../../database/models');
const mockMeds = require('../mock/medication.mock');
const mockUsers = require('../mock/user.mock');

const baseUrl = '/api/v1';

const medUser = { token: null }; 
const newMed = { id: null}


describe('api/v1/medication/* endpoints', () => {
    describe('[POST] /api/v1/medication', () => {
      it('should create medication for logged in user and return 201 Created', async () => {
        const res = await request
          .post(`${baseUrl}/user/signup`)
          .set('Content-Type', 'application/json')
          .send(mockUsers.validInput3);
        expect(res.statusCode).toBe(201);
        expect(res.body.success).toEqual(true);
        expect(res.body.body.token);
        medUser.token = res.body.body.token;

        const med = await request
          .post(`${baseUrl}/medication`)
          .set('Content-Type', 'application/json')
          .send(mockMeds.validInput1)
          .set('token', medUser.token);
          newMed.id = med.body.body.id;
        expect(med.status).toBe(201);
        expect(med.statusCode).toBe(201);
        expect(med.body.success).toEqual(true);
        expect(med.body.message).toEqual('Medication Created Successfully')
      });
  
      it('should throw an error if any field is empty', async () => {
        const res = await request
          .post(`${baseUrl}/medication`)
          .set('Content-Type', 'application/json')
          .send(mockMeds.emptyData)
          .set('token', medUser.token);
        expect(res.statusCode).toBe(400);
        expect(res.body.success).toEqual(false);
        expect(res.body.errors.message).toEqual('name is required');
      });
    });


    describe('[GET] Medication for logged in users', ()=>{
      it('should return 200 OK', async () => {
        const res = await request
          .get(`${baseUrl}/medication`)
          .set('Content-Type', 'application/json')
          .set('token', medUser.token);
        expect(res.statusCode).toBe(200);
        expect(res.body.success).toEqual(true);
      });

      it('should  get med by id and return 200 OK', async () => {
        const res = await request
          .get(`${baseUrl}/medication/${newMed.id}`)
          .set('Content-Type', 'application/json')
          .set('token', medUser.token);
        expect(res.statusCode).toBe(200);
        expect(res.body.success).toEqual(true);
      });
    })
  });