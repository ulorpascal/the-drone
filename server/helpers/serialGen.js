const { customAlphabet } = require('nanoid');
const saltRounds = process.env.SERIAL_LENGTH || 10
const nanoid = customAlphabet('1234567890abcdefghijklmnopqrstuvwxyz', saltRounds)

module.exports = {
    nanoid
}