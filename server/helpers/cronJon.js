const cron = require('node-cron');
require('dotenv').config();
const models = require('../database/models');
const auditLog= require('../helpers/auditLog');
const Logger = require('../config/winston.config');


const cronJon = async () => {
  cron.schedule('1,2,3,4,5 * * * * *', async () => {
    Logger.info("Cron run every 1,2,3,4,5 minutes")
    // TODO get all drones
    const drones = await models.Drones.findAll({ raw: true });
    const processLogs = await drones.map(item => {
      const output = {};
      output.droneId = item.id;
      output.level = item.batteryCapacity;
      if(item.batteryCapacity > 25){
        output.status = 'ACTIVE';
      }
      return output;
    });
    return auditLog(processLogs);
  });
};

module.exports = cronJon;
