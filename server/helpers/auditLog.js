require('dotenv').config();
const models = require('../database/models');
const Logger = require('../config/winston.config');


const auditLog = async(payload)=>{
    const Log = await models.Audit.bulkCreate(payload);
    Logger.info(payload)
    return Log
}


module.exports = auditLog;