const { Joi, joiValidate } = require('../helpers/joiValidate');
const toTitleCase = require('../helpers/toTitleCase');

/**
 * Validations
 *
 * @class Validations
 */
class Validations {
  /**
   * validate signup
   *
   * @static
   * @param {*} req
   * @return {*}
   * @memberof Validations
   */
  static async validateSignup(req, res, next) {
    const payload = {
      ...req.body, ...req.query, ...req.params, ...req.headers,
    };
    const schema = Joi.object().keys({
      username: Joi.string().required()
        .messages({
          'string.pattern.base': 'username must be a string',
          'any.required': 'username is not allowed to be empty',
        }),
      email: Joi.string().email().required().messages({
        'string.pattern.base': 'email must be a valid email',
        'any.required': 'email is not allowed to be empty',
      }),
      password: Joi.string().required().messages({
        'string.pattern.base': 'value must be a string',
        'any.required': 'value is not allowed to be empty',
      }),
    });
    req.payload = await joiValidate(payload, schema, req, res, next);
    return next();
  }

  /**
   * validate login
   *
   * @static
   * @param {*} req
   * @return {*}
   * @memberof Validations
   */
  static async validateLogin(req, res, next) {
    const payload = {
      ...req.body, ...req.query, ...req.params, ...req.headers,
    };
    const schema = Joi.object().keys({
      email: Joi.string().email().required().messages({
        'string.pattern.base': 'email must be a valid email',
        'any.required': 'email is not allowed to be empty',
      }),
      password: Joi.string().required().messages({
        'string.pattern.base': 'value must be a string',
        'any.required': 'password is not allowed to be empty',
      }),
    });
    req.payload = await joiValidate(payload, schema, req, res, next);
    return next();
  }

  /**
   * validate drone
   *
   * @static
   * @param {*} req
   * @return {*}
   * @memberof Validations
   */
   static async validateDrone(req, res, next) {
    const payload = {
      ...req.body, ...req.query, ...req.params, ...req.headers,
      model: req.body.model && toTitleCase(req.body.model),
      state: req.body.state && req.body.state.toUpperCase(),
    };
    const schema = Joi.object().keys({
      model: Joi.string().valid('Lightweight', 'Middleweight', 'Cruiserweight', 'Heavyweight').default('Lightweight').messages({
        'string.pattern.base': "model must be a string",
        'any.only': `model value must be one of the following Lightweight, Middleweight, Cruiserweight, Heavyweight`,
        'any.required': 'model name is not allowed to be empty'
    }),
      weightLimit: Joi.number().max(500).precision(2).required(),
      batteryCapacity: Joi.number().max(100).required(),
      state:  Joi.string().valid('IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING').default('IDLE').messages({
        'string.pattern.base': "state must be a string",
        'any.only': `state value must be one of the following IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING`,
        'any.required': 'state name is not allowed to be empty'
      }),
    });
    req.payload = await joiValidate(payload, schema, req, res, next);
    return next();
  }


  /**
   * Get Drone validation
   *
   * @static
   * @param {*} req
   * @return {*}
   * @memberof Validations
   */
   static async GetDrone(req, res, next) {
    const payload = {
      ...req.body, ...req.query, ...req.params, ...req.headers,
      model: req.query.model && toTitleCase(req.query.model),
      state: req.query.state && req.query.state.toUpperCase(),
      page: req.query.page && req.query.page * 1 || 1,
      weightLimit: req.query.weightLimit && req.query.weightLimit * 1,
      batteryCapacity: req.query.batteryCapacity && req.query.batteryCapacity * 1,
      droneId: req.params.droneId && req.params.droneId * 1,
    };
    const schema = Joi.object().keys({
      model: Joi.string().valid('Lightweight', 'Middleweight', 'Cruiserweight', 'Heavyweight').optional().allow('', null).messages({
        'string.pattern.base': "model must be a string",
        'any.only': `model value must be one of the following Lightweight, Middleweight, Cruiserweight, Heavyweight`,
        'any.required': 'model name is not allowed to be empty'
    }),
      weightLimit: Joi.number().precision(2).optional().allow('', null),
      batteryCapacity: Joi.number().max(100).optional().allow('', null),
      state:  Joi.string().valid('IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING').optional().allow('', null).messages({
        'string.pattern.base': "state must be a string",
        'any.only': `state value must be one of the following IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING`,
        'any.required': 'state name is not allowed to be empty'
      }),
      page: Joi.number().optional().allow('', null),
      droneId: Joi.number().optional().allow('', null),
    });
    req.payload = await joiValidate(payload, schema, req, res, next);
    return next();
  }


  /**
   * validate medication
   *
   * @static
   * @param {*} req
   * @return {*}
   * @memberof Validations
   */
   static async validateMedication(req, res, next) {
    const payload = {
      ...req.body, ...req.query, ...req.params, ...req.headers, ...req.file,
      weight: req.body.weight && req.body.weight * 1,
    };
    const schema = Joi.object().keys({
      name: Joi.string().regex(/[a-zA-Z0-9_\-\+\.]+/)
      .message('Prefix should only contain alphanumeric characters').required(),
      weight: Joi.number().precision(2).required(),
      code: Joi.string().regex(/[A-ZA-Z0-9_\-\+\.]+/)
      .message('Prefix should only contain uppercase alphabets, numeric characters and underscores').required(),
      image:  Joi.string().messages({
        'string.pattern.base': "state must be a string",
        'any.required': 'image is not allowed to be empty'
      }),
    });
    req.payload = await joiValidate(payload, schema, req, res, next);
    return next();
  }

  /**
   * validate medication
   *
   * @static
   * @param {*} req
   * @return {*}
   * @memberof Validations
   */
   static async GetMedication(req, res, next) {
    const payload = {
      ...req.body, ...req.query, ...req.params, ...req.headers,
      page: req.query.page && req.query.page * 1 || 1,
      weight: req.query.weight && req.query.weight * 1,
      medId: req.params.medId && req.params.medId * 1
    };
    const schema = Joi.object().keys({
      name: Joi.string().optional().allow('', null),
      weight: Joi.number().optional().allow('', null),
      code: Joi.string().optional().allow('', null),
      medId: Joi.number().optional().allow('', null),
    });
    req.payload = await joiValidate(payload, schema, req, res, next);
    return next();
  }



  /**
   * validate dispatch
   *
   * @static
   * @param {*} req
   * @return {*}
   * @memberof Validations
   */
   static async LoadDrone(req, res, next) {
    const payload = {
      ...req.body, ...req.query, ...req.params, ...req.headers,
    };
    const schema = Joi.object().keys({
      medCode: Joi.string().regex(/[A-ZA-Z0-9_\-\+\.]+/)
      .message('Prefix should only contain uppercase alphabets, numeric characters and underscores').required(),
      droneSerialNumber: Joi.string().max(100).required(),
    });
    req.payload = await joiValidate(payload, schema, req, res, next);
    return next();
  }


  /**
   * validate Audit
   *
   * @static
   * @param {*} req
   * @return {*}
   * @memberof Validations
   */
   static async GetAudit(req, res, next) {
    const payload = {
      ...req.body, ...req.query, ...req.params, ...req.headers,
      status: req.query.status && req.query.status.toUpperCase(),
      page: req.query.page && req.query.page * 1 || 1,
      droneId: req.params.droneId && req.params.droneId * 1,
      level: req.query.level && req.query.level * 1,
    };
    const schema = Joi.object().keys({
      droneId: Joi.number().optional().allow('', null),
      status: Joi.string().valid('ACTIVE', 'INACTIVE').optional().allow('', null).messages({
        'string.pattern.base': "status must be a string",
        'any.only': `status value must be one of the following ACTIVE, INACTIVE`,
        'any.required': 'status name is not allowed to be empty'
      }),
      level: Joi.number().max(100).optional().allow('', null),
    });
    req.payload = await joiValidate(payload, schema, req, res, next);
    return next();
  }
}

module.exports = Validations;
