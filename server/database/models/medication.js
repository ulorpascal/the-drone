/** @description Medication database model with foreign keys and associations
 * @param  {obj} sequelize
 * @param  {obj} DataTypes
 * @returns {obj} Medication model
 */

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Medication extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Medication.belongsTo(models.User, {
        foreignKey: 'userId',
      })
      Medication.hasMany(models.Dispatch, {
        foreignKey: 'medId',
      })
    }
  }
  Medication.init({
    name: {
      type: DataTypes.STRING,
    },
    code: {
      type: DataTypes.STRING,
    },
    weight: {
      type: DataTypes.DOUBLE,
    },
    image: {
      type: DataTypes.JSONB,
    },
    userId: {
      type: DataTypes.STRING,
    }
  }, {
    sequelize,
    modelName: 'Medication',
  });
  return Medication;
};
