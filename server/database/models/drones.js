const { nanoid } = require('../../helpers/serialGen');

/** @description Drones database model with foreign keys and associations
 * @param  {obj} sequelize
 * @param  {obj} DataTypes
 * @returns {obj} Drones model
 */

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Drones extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Drones.belongsTo(models.User, {
        foreignKey: 'userId',
      })
      Drones.hasMany(models.Dispatch, {
        foreignKey: 'droneId',
      })
    }
  }
  Drones.init({
    serialNumber: {
      type: DataTypes.STRING(100),
    },
    model: {
      type: DataTypes.ENUM(['Lightweight', 'Middleweight', 'Cruiserweight', 'Heavyweight']),
      defaultValue: 'Lightweight',
    },
    weightLimit: {
      type: DataTypes.DOUBLE(3, 2),
    },
    batteryCapacity: {
      type: DataTypes.DOUBLE,
    },
    state: {
      type: DataTypes.ENUM(['IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING']),
      defaultValue: 'IDLE'
    },
    userId: {
      type: DataTypes.STRING,
    },
    loadCapacity: {
      type: DataTypes.DOUBLE,
      defaultValue: 0
    },
  }, {
    sequelize,
    modelName: 'Drones',
  });

  Drones.beforeCreate(async (drone) => {
    // eslint-disable-next-line no-param-reassign
    drone.serialNumber = nanoid();
  });
  return Drones;
};
