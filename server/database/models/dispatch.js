'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Dispatch extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Dispatch.belongsTo(models.User, {
        foreignKey: 'userId',
      })
      Dispatch.belongsTo(models.Drones, {
        foreignKey: 'droneId',
      })
      Dispatch.belongsTo(models.Medication, {
        foreignKey: 'medId',
      })
    }
  }
  Dispatch.init({
    userId: {
      type: DataTypes.STRING
    },
    droneId: {
      type: DataTypes.INTEGER
    },
    medId: {
      type: DataTypes.INTEGER
    },
  }, {
    sequelize,
    modelName: 'Dispatch',
  });
  return Dispatch;
};