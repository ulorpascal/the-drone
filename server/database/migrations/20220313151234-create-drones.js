module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Drones', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      serialNumber: {
        type: Sequelize.STRING,
        unique: true,
      },
      model: {
        type: Sequelize.ENUM,
        values: ['Lightweight', 'Middleweight', 'Cruiserweight', 'Heavyweight'],
        defaultValue: 'Lightweight',
      },
      weightLimit: {
        type: Sequelize.DOUBLE,
      },
      batteryCapacity: {
        type: Sequelize.DOUBLE,
      },
      state: {
        type: Sequelize.ENUM,
        values: ['IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING'],
        defaultValue: 'IDLE',
      },
      userId: {
        type: Sequelize.STRING,
      },
      loadCapacity: {
        type: Sequelize.DOUBLE,
        defaultValue: 0
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Drones');
  },
};
